using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace TiltBrush {

public class Plugin {
	public Plugin() {
		Console.Out.WriteLine("my plugin STROKE TEST");
		var brush = PointerManager.m_Instance.MainPointer.CurrentBrush;
		uint time = 0;
		float minPressure = PointerManager.m_Instance.MainPointer.CurrentBrush.PressureSizeMin(false);
		float defaultPressure = Mathf.Lerp(minPressure, 1f, 0.5f);
		Console.Out.WriteLine(minPressure);
		Console.Out.WriteLine(defaultPressure);

		var group = App.GroupManager.NewUnusedGroup();
		var controlPoints = new List<PointerManager.ControlPoint>();


		float size = 40.0f;
		for (var i=0; i < 360; i++) {
			var x = Mathf.Sin( i * Mathf.Deg2Rad );
			var z = Mathf.Cos( i * Mathf.Deg2Rad );
			var vec = new Vector3(x * size, 0.0f, z * size);
			controlPoints.Add(
				new PointerManager.ControlPoint(){
					m_Pos = vec,
					m_Orient = new Quaternion(),
					m_Pressure = 1.0f,
					m_TimestampMs = time++
				}
			);
		}

		var stroke = new Stroke {
			m_Type = Stroke.Type.NotCreated,
			m_IntendedCanvas = App.Scene.ActiveCanvas,
			m_BrushGuid = brush.m_Guid,
			m_BrushScale = 1f,
			m_BrushSize = PointerManager.m_Instance.MainPointer.BrushSizeAbsolute,
			m_Color = App.BrushColor.CurrentColor,
			m_Seed = 0,
			m_ControlPoints = controlPoints.ToArray(),
		};
		stroke.m_ControlPointsToDrop = Enumerable.Repeat(false, stroke.m_ControlPoints.Length).ToArray();
		stroke.Group = @group;
		stroke.Recreate(null, App.Scene.ActiveCanvas);


		SketchMemoryScript.m_Instance.MemoryListAdd(stroke);
		SketchMemoryScript.m_Instance.PerformAndRecordCommand(
			new BrushStrokeCommand(stroke, WidgetManager.m_Instance.ActiveStencil, 123)
		);


	}
}

} // namespace
