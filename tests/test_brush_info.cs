using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plugin {
	public Plugin() { 
		Console.Out.WriteLine("my plugin CURRENT BRUSH INFO TEST");
		var brush = TiltBrush.PointerManager.m_Instance.MainPointer.CurrentBrush;
		uint time = 0;
		float minPressure = TiltBrush.PointerManager.m_Instance.MainPointer.CurrentBrush.PressureSizeMin(false);
		float defaultPressure = Mathf.Lerp(minPressure, 1f, 0.5f);
		Console.Out.WriteLine(minPressure);
		Console.Out.WriteLine(defaultPressure);
	}
}
