#!/usr/bin/python3
import os, sys, platform, subprocess, urllib, time, json
import urllib.request
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton, QLineEdit, QTextEdit

APP_WIDTH = 960
APP_HEIGHT = 420
TOOLBAR_HEIGHT = 90
XOPENBRUSH = None

# notes:
# on ubuntu you need to install:
# sudo apt-get install libgconf-2-4

def count_lines( folder, info ):
	for name in os.listdir(folder):
		path = os.path.join( folder, name)
		if os.path.isdir( path ):
			count_lines( path, info )
		elif name.endswith( '.cs' ):
			data = open(path, 'rb').read().decode('utf-8')
			lines = data.splitlines()
			info['lines'] += len( lines )
			for ln in lines:
				if ln.strip():
					info['no-whitespace'] += 1
		elif name.endswith( '.shader' ):
			data = open(path, 'rb').read().decode('utf-8')
			lines = data.splitlines()
			info['shader-lines'] += len( lines )
			for ln in lines:
				if ln.strip():
					info['shader-no-whitespace'] += 1
		
def do_count_lines():
	info = {'lines':0,'no-whitespace':0, 'shader-lines':0, 'shader-no-whitespace':0}
	count_lines( './Assets', info )
	print(info)

if '--loc' in sys.argv:
	do_count_lines()

# Add ../Python to sys.path
sys.path.append(
	os.path.join(os.path.dirname(os.path.abspath(__file__)), "Support/Python")
)

exe = os.path.expanduser('~/Builds/Monoscopic_Release_OpenBrush_Experimental_FromCli/OpenBrush')

if '--build' in sys.argv or not os.path.isfile(exe):
	print(__file__)
	print(sys.path)
	import unitybuild
	print(unitybuild)	
	import unitybuild.main  # noqa: E402 pylint: disable=import-error,wrong-import-position
	args = ["--experimental", "--vrsdk", "Monoscopic", '--platform', 'Linux']
	unitybuild.main.main(args)

BOOTCS = [
	#'./tests/helloworld_plugin.cs',
	#'./tests/test_window_resize.cs',
	#'./tests/test_make_cube.cs',
	#'./tests/test_brush_info.cs',
	#'./tests/test_draw_strokes.cs',
	'./tests/test_anim_circle.cs',
]

OBPROC = None
def run_openbrush():
	global OBPROC
	#cmd = [exe, test]   ## note, can not pass a cs file as a command line arg like this
	##                   ## because OpenBrush thinks its a .tilt file for loading and crashes
	cmd =[exe]
	print(cmd)
	if OBPROC:
		OBPROC.kill()
	OBPROC = subprocess.Popen( cmd )
	
def run_mcs():
	for idx, startup in enumerate(BOOTCS):
		test = '/tmp/mytest%s.cs' % idx
		data = open(startup, 'rb').read()
		open(test, 'wb').write( data )
		url = 'http://localhost:40074/compile?%s' % test
		print(url)
		f = urllib.request.urlopen(url)
		print(f.read().decode('utf-8'))
		time.sleep(4)

def get_log_path():
	return os.path.expanduser('~/.config/unity3d/Icosa/Open Brush/Player.log')

def open_log():
	log = get_log_path()
	subprocess.Popen(['gedit', log])


import Xlib, Xlib.display, Xlib.Xutil

# Connect to the X server and get the root window
XDISP = Xlib.display.Display()
XROOT = XDISP.screen().root
XBlender = None

def get_all_windows(window, windows=None):
	if windows is None:
		windows = {}
	children = window.query_tree().children
	if not children:
		return windows
	for w in children:
		tag = None
		wclass = w.get_wm_class()
		if wclass:
			wclass, tag = wclass
		name = w.get_wm_name()
		if '--debug' in sys.argv:
			print(name, wclass, tag)
		if not name and wclass:
			name = wclass
		windows[(name,wclass,tag)] = w
		children = w.query_tree().children
		if children:
			get_all_windows(w, windows=windows)

	return windows


def get_xwindow(exename=None, title=None, tag=None ):
	wins = get_all_windows(XROOT)
	#raise RuntimeError(wins)
	for wclass in wins:
		a,b,c = wclass
		if exename and exename==a:
			return wins[wclass]
		elif title and title==b:
			return wins[wclass]
		elif tag and tag==c:
			return wins[wclass]



def lock_external_windows():
	xwin = None
	while not xwin:
		xwin = get_xwindow( tag='OpenBrush' )
		time.sleep(0.2)
	if not xwin:
		print("WARN: could not get OpenBrush XWindow")
		return
	global XOPENBRUSH, XBlender, XQTimer
	XBlender = get_xwindow(tag='Blender')
	XOPENBRUSH = xwin

	update_xwindow()

	set_window_hints(XOPENBRUSH, hide_title_bar=True)
	if XBlender:
		set_window_hints(XBlender, hide_title_bar=True)


	XQTimer = QTimer()
	XQTimer.setInterval(30)
	XQTimer.timeout.connect( update_xwindow )
	XQTimer.start()

def set_window_hints( win, urgent=False, hide_title_bar=False ):
	hints = win.get_wm_hints() or { 'flags': 0 }
	if urgent:
		hints['flags'] |= Xutil.UrgencyHint
		win.set_wm_hints(hints)
	if hide_title_bar:
		#hints['flags'] |= Xutil.UrgencyHint
		subprocess.check_call([
			'xprop',
			'-name', win.get_wm_name(),
			'-format',
			'_MOTIF_WM_HINTS',
			'32i',
			'-set',
			'_MOTIF_WM_HINTS',
			'2',
		])



XPREV = [-1,-1]
XPREV_BLENDER = [-1,-1]
APPMODE = 'SPLIT'
BlenderWidgets = []
OpenBrushWidgets = []

def app_set_mode( mode ):
	global APPMODE
	APPMODE = mode

	if APPMODE == 'OPENBRUSH':
		for w in BlenderWidgets:
			w.hide()
		for w in OpenBrushWidgets:
			w.show()
	elif APPMODE == 'SPLIT':
		for w in BlenderWidgets:
			w.hide()
		for w in OpenBrushWidgets:
			w.hide()

	elif APPMODE == 'BLENDER':
		for w in BlenderWidgets:
			w.show()
		for w in OpenBrushWidgets:
			w.hide()


	update_xwindow( force=True )

def update_xwindow( force=False ):
	fudge = 37   ## depends on window manager?

	obrush_width = APP_WIDTH
	if XBlender:
		if APPMODE == 'OPENBRUSH':
			obrush_width = APP_WIDTH - 320
			blender_x = obrush_width
			blender_width = 320
		elif APPMODE == 'BLENDER':
			obrush_width = 320
			blender_width = APP_WIDTH - 320
			blender_x = obrush_width
		else:
			obrush_width = int( APP_WIDTH / 2 )
			blender_x = obrush_width
			blender_width = obrush_width


	x = MAINWIN.x()
	y = MAINWIN.y()
	X,Y = XPREV
	dirty = False
	if x != X or y != Y or force:
		if XOPENBRUSH:
			XOPENBRUSH.configure(x=x, y=y+TOOLBAR_HEIGHT+fudge, width=obrush_width, height=APP_HEIGHT)
			XPREV[0] = x
			XPREV[1] = y
			dirty = True

	if XBlender:
		X,Y = XPREV_BLENDER
		if x != X or y != Y or force:
			offset = int(APP_WIDTH / 2)
			XBlender.configure(x=x+blender_x, y=y+TOOLBAR_HEIGHT+fudge, width=blender_width, height=APP_HEIGHT)
			XPREV_BLENDER[0] = x
			XPREV_BLENDER[1] = y
			dirty = True

	if dirty:
		XDISP.sync()

BPY_SCRIPT = r'''
import os, sys, bpy, threading, json, time
bpy.context.preferences.view.show_splash = False

def thread_loop():
	while True:
		msg = sys.stdin.readline()
		#try:
		msg = json.loads( msg )
		#except:
		#	#time.sleep(1)
		#	print('json message error')
		#	continue
		print('got new msg:', msg)
		if 'eval' in msg:
			exec( msg['eval'] )
		time.sleep(0.01)

threading._start_new_thread( thread_loop, tuple([]) )

'''

BLENDERPROC = None
def run_blender():
	global BLENDERPROC
	BLENDER_EXE = 'blender'

	tmp = '/tmp/openbrush_bpy.py'
	open(tmp, 'wb').write( BPY_SCRIPT.encode('utf-8') )
	cmd = [BLENDER_EXE, '--window-geometry', '640', '480', '100', '100', '--python', tmp]
	BLENDERPROC = subprocess.Popen(cmd, stdin=subprocess.PIPE)

def run_qt_debug():
	global MAINWIN
	app = QApplication(sys.argv)
	widget = QWidget()
	MAINWIN = widget
	textLabel = QLabel(widget)
	textLabel.setText("OpenBrushQT")
	textLabel.move(10,5)
	
	btn = QPushButton( 'run openbrush', widget )
	btn.clicked.connect( run_openbrush )
	btn.move(110,5)

	btn = QPushButton( 'mcs tests', widget )
	btn.clicked.connect( run_mcs )
	btn.move(260,5)

	btn = QPushButton( 'run blender', widget )
	btn.clicked.connect( run_blender )
	btn.move(APP_WIDTH - 100,5)

	btn = QPushButton( 'toolbar', widget )
	btn.clicked.connect( lock_external_windows )
	btn.move(30,50)
	
	btn = QPushButton( 'debug log', widget )
	btn.clicked.connect( open_log )
	btn.move(APP_WIDTH - 200,5)

	widget.setGeometry(50,50,APP_WIDTH,TOOLBAR_HEIGHT)
	widget.setWindowTitle("OpenBrushQT")
	widget.show()
	sys.exit(app.exec_())


class OpenBrushQt( QWidget ):
	def closeEvent(self, event):
		#widgetList = QApplication.topLevelWidgets()
		#numWindows = len(widgetList)
		#if numWindows > 1:
		#event.ignore()
		#else:
		if BLENDERPROC:
			try:
				BLENDERPROC.kill()
			except:
				pass
		if OBPROC:
			try:
				OBPROC.kill()
			except:
				pass
		event.accept()

CONSOLE = None
CSCONSOLE = None
def on_macro_button( btn ):
	txt = btn.text()
	if txt == '_':
		btn.script = CONSOLE.text()
		btn.setText('⏩')

	if BLENDERPROC:
		print('blender python eval >>>', btn.script)
		msg = {
			'eval' : btn.script
		}
		msg = json.dumps( msg )
		BLENDERPROC.stdin.write( msg.encode('utf-8') + b'\n' )
		BLENDERPROC.stdin.flush()
	else:
		eval(btn.script)

def on_cs_compile():
	cs = [
		"using System;",
		"using System.Diagnostics;",
		"using System.Collections;",
		"using System.Collections.Generic;",
		"using UnityEngine;",
		'public class Plugin {',
		'	public Plugin() {',
		'		' + CSCONSOLE.text(),
		'	}',
		'}'
	]
	cs = '\n'.join( cs )
	print('C# MCS compile:', cs)
	tmp = '/tmp/eval_%s.cs' % time.time()
	open(tmp, 'wb').write( cs.encode('utf-8') )

	url = 'http://localhost:40074/compile?%s' % tmp
	print(url)
	f = urllib.request.urlopen(url)
	ret = f.read().decode('utf-8')
	print(ret)
	return ret

PREVLOG = None
LOGDEBUG = {'lines':[], 'start':-1, 'end':-1, 'index':-1}
def update_main():
	global PREVLOG
	log = get_log_path()
	if os.path.isfile( log ):
		txt = OpenBrushWidgets[0]
		data = open(log, 'rb').read().decode('utf-8')
		if data != PREVLOG:
			PREVLOG = data
			lines = data.strip().splitlines()
			nlines = []
			for ln in lines:
				if ln.strip():
					nlines.append( ln )
			lines = nlines

			LOGDEBUG['lines'] = lines
			n = len( LOGDEBUG['lines'] )
			LOGDEBUG['start'] = n - 30
			LOGDEBUG['end'] = n
			LOGDEBUG['index'] = LOGDEBUG['start']

	if len( LOGDEBUG['lines'] ):
		LOGDEBUG['index'] += 1
		if LOGDEBUG['index'] >= LOGDEBUG['end']:
			LOGDEBUG['index'] = LOGDEBUG['start']
		line = LOGDEBUG['lines'][ LOGDEBUG['index'] ]
		txt.setText( 
			'%s : %s' % ( LOGDEBUG['index']+1, line.strip() )

		)

CS_EDITOR = {}

def on_cs_compile_editor():
	cs = CS_EDITOR['editor'].toPlainText()
	print('C# MCS compile:', cs)
	tmp = '/tmp/script_%s.cs' % time.time()
	open(tmp, 'wb').write( cs.encode('utf-8') )
	url = 'http://localhost:40074/compile?%s' % tmp
	print(url)
	f = urllib.request.urlopen(url)
	ret = f.read().decode('utf-8')
	print(ret)
	return ret


def open_cs_editor():
	width = 800
	widget = QWidget()
	CS_EDITOR['window'] = widget

	btn = QPushButton( 'compile', widget )
	btn.clicked.connect( on_cs_compile_editor )
	btn.move(10,5)

	example = open('tests/test_draw_circle.cs', 'rb').read().decode('utf-8')
	edit = QTextEdit( widget )
	edit.setPlainText( example )
	edit.setStyleSheet('font-size:9px')
	CS_EDITOR['editor'] = edit
	edit.move(10, 40)
	edit.setFixedWidth( width-20 )
	edit.setFixedHeight( 450 )
	widget.setGeometry(400,50, width, 500)
	widget.setWindowTitle("C# Editor")
	widget.show()



def run_qt():
	global MAINWIN, CONSOLE, CSCONSOLE, MAINTIMER
	app = QApplication(sys.argv)
	widget = OpenBrushQt()
	MAINWIN = widget
	textLabel = QLabel(widget)
	textLabel.setText("OpenBrushQT")
	textLabel.move(10,5)

	textLabel = QLabel(widget)
	textLabel.setText(">>>")
	textLabel.move(10,70)
	OpenBrushWidgets.append( textLabel )


	center = int( APP_WIDTH / 2	)
	btn = QPushButton( '🔶', widget )
	btn.clicked.connect( lambda: app_set_mode('OPENBRUSH') )
	btn.move( center - 64, 5 )

	btn = QPushButton( '💠', widget )
	btn.clicked.connect( lambda: app_set_mode('SPLIT') )
	btn.move( center, 5 )

	btn = QPushButton( '🔷', widget )
	btn.clicked.connect( lambda: app_set_mode('BLENDER') )
	btn.move( center + 64, 5 )


	btn = QPushButton( 'mcs tests', widget )
	btn.clicked.connect( run_mcs )
	btn.move(260,5)
	OpenBrushWidgets.append( btn )

	
	btn = QPushButton( 'debug log', widget )
	btn.clicked.connect( open_log )
	btn.move(170,5)
	OpenBrushWidgets.append( btn )

	textLabel = QLabel(widget)
	textLabel.setText("py")
	textLabel.move(10,42 )
	BlenderWidgets.append( textLabel )

	input = QLineEdit( widget )
	CONSOLE = input
	input.move( 40, 40 )
	input.setFixedWidth( APP_WIDTH - 80 )
	#input.setText('print("hello world")')
	input.setText("bpy.data.objects['Cube'].rotation_euler.x += 1")
	BlenderWidgets.append( input )


	textLabel = QLabel(widget)
	textLabel.setText("C#")
	textLabel.move(10,42 )
	OpenBrushWidgets.append( textLabel )

	input = QLineEdit( widget )
	input.returnPressed.connect( on_cs_compile )
	CSCONSOLE = input
	input.move( 40, 40 )
	input.setFixedWidth( APP_WIDTH - 200 )
	#input.setText('print("hello world")')
	input.setText('Console.Out.WriteLine("hello from unityMCS");')
	OpenBrushWidgets.append( input )

	btn = QPushButton( '📝', widget )
	btn.move( APP_WIDTH - 100, 40 )
	btn.clicked.connect( open_cs_editor )
	OpenBrushWidgets.append( btn )


	o = APP_WIDTH - 350
	for i in range(10):
		btn = QPushButton( '_', widget )
		BlenderWidgets.append( btn )

		btn.script = ''
		btn.clicked.connect( lambda state, button=btn: on_macro_button(button) )
		btn.move(o,70)
		btn.setFixedWidth( 32 )
		o += 32


	widget.setGeometry(50,50,APP_WIDTH,TOOLBAR_HEIGHT)
	widget.setWindowTitle("OpenBrushQT")
	widget.show()

	for w in OpenBrushWidgets + BlenderWidgets:
		w.hide()

	if '--test-qt' in sys.argv:
		pass
	else:
		run_blender()
		run_openbrush()
		time.sleep(5)
		lock_external_windows()

	MAINTIMER = QTimer()
	MAINTIMER.setInterval(1000)
	MAINTIMER.timeout.connect( update_main )
	MAINTIMER.start()

	sys.exit(app.exec_())


if '--debug' in sys.argv:
	run_qt_debug()
else:
	run_qt()


